const indexedDB = this.indexedDB || this.mozIndexedDB || this.webkitIndexedDB || this.msIndexedDB;
const IDBKeyRange = this.IDBKeyRange || this.webkitIDBKeyRange || this.msIDBKeyRange;

const DB_NAME = 'METEO_DATA';

/**
 * Обработка входящих сообщений, ожидается что у всех сообщений будет обезаетльное поле type
 * @param e
 */
onmessage = (e) => {
    const state = e.data.state;
    switch (e.data.type) {
        case 'GET_DATA': {
            getDataFromDb(state).then((storedData) => {
                if (!storedData.length) {
                    return getFromServer(state.graphType)
                        .then((dataFromServer) => {
                            const dataForStore = prepareDataForStore(dataFromServer);
                            return setDataToDb(state, dataForStore);
                        }).then(() => {
                            return getDataFromDb(state);
                        }).then((data) => {
                            return sendResultData(data);
                        });
                } else {
                    return sendResultData(storedData);
                }
            });
            break;
        }
        default: {
            throw Error('Незивестное событие');
        }
    }
};


function sendResultData(data) {
    postMessage({type: 'RESULT_DATA', result: data});
}

/**
 * Обертка подключения к БД
 * @param {string} objectStoreName имя стора с которым требуется работать
 * @returns {Promise<IDBObjectStore>} результат открытия, при удаче отправлет
 */
function openDb(objectStoreName) {
    let request = indexedDB.open(DB_NAME);

    request.onupgradeneeded = () => {
        const temperatureStore = request.result.createObjectStore('TEMPERATURE', {autoIncrement: true, keyPath: 'key'});
        temperatureStore.createIndex('year', 'year', {unique: false});
        const precipitationStore = request.result.createObjectStore('PRECIPITATION', {
            autoIncrement: true,
            keyPath: 'key'
        });
        precipitationStore.createIndex('year', 'year', {unique: false});
    };
    return new Promise((resolve, reject) => {
        request.onsuccess = () => {
            const db = request.result;
            const transaction = db.transaction(objectStoreName, 'readwrite');
            const objectStore = transaction.objectStore(objectStoreName);
            resolve(objectStore);
        };
        request.onerror = () => {
            reject(Error('Ошибка открытия базы'));
        };
    });
}

/**
 * Получение данных из БД в выборку поподают только данные за выбранный промежуток времени
 * @param state
 * @return {Promise<Array>} массив данных по месяцам за указанный отрезок времени
 */
function getDataFromDb(state) {
    return openDb(state.graphType).then((store) => {
        return new Promise((resolve) => {
            const index = store.index('year');
            const request = index.getAll(IDBKeyRange.bound(+state.startDate, +state.endDate));
            request.onsuccess = function (event) {
                let storedData = event.target.result;
                resolve(storedData);
            };
        });
    });
}

/**
 * Сохранение данных БД
 * @param state
 * @param data данные оп месяцам
 * @return {Promise<any[]>} возвращаем все обработанные записи
 */
function setDataToDb(state, data) {
    const resultStorePromises = [];
    openDb(state.graphType).then((objectStore) => {
        data.forEach((monthData) => {
            resultStorePromises.push(new Promise((resolve, reject) => {
                const request = objectStore.put(monthData);
                request.onsuccess = () => resolve(monthData);
                request.onerror = () => reject(Error('Не удалось сохранить данные'));
            }));
        });
    });
    return Promise.all(resultStorePromises);
}

/**
 * Получение данных с сервера
 * @param {string} type тип запрашиваеммых данных (температура/осадки)
 * @return {Promise<Response>}
 */
function getFromServer(type) {
    let apiUrl = '/api/temperature';
    if (type === 'PRECIPITATION') {
        apiUrl = '/api/precipitation';
    }
    return fetch(this.location.origin + apiUrl, {
        method: 'GET',
        mode: 'no-cors',
        cache: 'default'
    }).then((response) => {
        return response.json();
    });
}

/**
 * Преборазуем данные для хранения а БД согласно требованию
 * Запись за отдельный месяц метеорологических измерений должна хранится как отдельный объект/запись в IndexedDB.
 * @param {Array} data массив данных полученных с сервера
 * @return {Array}
 */
function prepareDataForStore(data) {
    return data.reduce(function (result, entry) {
        const date = entry.t.split('-');
        const year = date[0];
        const month = date[1];
        let lastResult = result[result.length - 1];
        if (!lastResult) {
            lastResult = {key: `${year}-${month}`, year: +year, month: +month, values: []};
            result.push(lastResult);
        }
        if (lastResult.year === +year && lastResult.month === +month) {
            lastResult.values.push(+entry.v);
        } else {
            result.push({key: `${year}-${month}`, year: +year, month: +month, values: [+entry.v]});
        }
        return result;
    }, []);
}