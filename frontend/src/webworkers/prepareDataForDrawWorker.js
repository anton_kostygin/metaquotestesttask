onmessage = (e) => {
    const state = e.data.state;
    const data = e.data.data;
    const size = e.data.size;
    switch (e.data.type) {
        case 'PREPARE_DATA': {
            prepare(state, data, size).then((dataForRender) => {
                postMessage({type: 'RESULT_DATA', result: dataForRender});
            });
            break;
        }
        default: {
            throw Error('Незивестное событие');
        }
    }
};


function prepare(state, data, size) {
    let max = Number.NEGATIVE_INFINITY;
    let min = Number.POSITIVE_INFINITY;

    // Преборазуем данные в плоскую структуру день-значение и служебные данные
    const resultData = data.reduce((result, monthData) => {
        monthData.values.forEach((value, index) => {
            result.push({
                key: `${monthData.key}-${(index + 1).toString().padStart(2, '0')}`,
                year: monthData.year,
                month: monthData.month,
                day: index + 1,
                value: value,
                x: null,
                y: null
            });
            if (value > max) {
                max = value;
            }
            if (value < min) {
                min = value;
            }
        });
        return result;
    }, []);

    // Выбираем только нужное количество данных,
    // т.е. на каждый горизонтальный пиксель берем максимальное и минимальное значение
    const daysInPixel = resultData.length / size.width;
    let resultDataSelected = [];
    // выбирать акутально если на каждый пиксель выпадет больше двух дней
    if (daysInPixel >= 3) {
        while (resultData.length) {
            const chunk = resultData.splice(0, daysInPixel);
            const totalChunkExtremum = chunk.reduce((extremum, day) => {
                if (extremum.min.value > day.value) {
                    extremum.min = day;
                }
                if (extremum.max.value < day.value) {
                    extremum.max = day;
                }
                return extremum;
            }, {max: {value: Number.NEGATIVE_INFINITY}, min: {value: Number.POSITIVE_INFINITY}});


            resultDataSelected.push(totalChunkExtremum.min, totalChunkExtremum.max);
        }
    } else {
        resultDataSelected = resultData;
    }

    const scale = calcScale(resultDataSelected, size.width, size.height, min, max);
    let offsetX = 0;


    // Для каждой точки графика рассчитываем "физические" координты для вывода на canvas
    resultDataSelected.forEach((day) => {
        const y = (-day.value * scale.multiplicatorY) + (scale.offsetY * scale.multiplicatorY) + size.height;
        const x = offsetX;
        day.y = y;
        day.x = x;
        offsetX += scale.stepInPixel;
    });

    return Promise.resolve({
        data: resultDataSelected,
        graphType: state.graphType,
        min: min,
        max: max,
        width: size.width,
        height: size.height
    });

}

/**
 * Рассчет множетелей и свдивгов для осей X и Y
 * @param data
 * @param width
 * @param height
 * @param min
 * @param max
 * @return {{}}
 */
function calcScale(data, width, height, min, max) {
    const result = {};
    result.stepInPixel = width / data.length;

    const delta = max - min;

    result.offsetY = min;
    result.multiplicatorY = ((height / delta) / 100) * 90;
    return result;
}