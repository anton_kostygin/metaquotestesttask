import {startApp} from './app/index.js';
import {initCanvas} from './canvas/index.js';
import {initSelects} from './utils/selectUtils.js';
import {getData} from './dataProvider/index.js';

const temperatureButton = document.querySelector('#temperature');
const precipitationButton = document.querySelector('#precipitation');
const startDateSelect = document.querySelector('#startDate');
const endDateSelect = document.querySelector('#endDate');
const canvasElem = document.querySelector('#canvas');

initSelects(startDateSelect, endDateSelect);

const canvas = initCanvas(canvasElem);

startApp(getData, canvas.draw, temperatureButton, precipitationButton, startDateSelect, endDateSelect);

