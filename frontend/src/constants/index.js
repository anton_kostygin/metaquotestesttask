export const START_DATE = 1881;
export const END_DATE = 2006;
export const PRECIPITATION = 'PRECIPITATION';
export const TEMPERATURE = 'TEMPERATURE';