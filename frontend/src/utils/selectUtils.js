import {END_DATE, START_DATE} from '../constants/index.js';


function createOptionsForDates(begin, end) {
    const options = [];
    for (let i = begin; i <= end; i++) {
        options.push(new Option(i, i));
    }
    return options;
}

function addOptionsToSelect(selectElem, options) {
    options.forEach((option) => {
        selectElem.add(option);
    });
}

function setSelectOptions(selectElem, startDate, endDate) {
    const options = createOptionsForDates(startDate, endDate);
    addOptionsToSelect(selectElem, options);
}

export function initSelects(startDateSelectElem, endDateSelectElem) {
    setSelectOptions(startDateSelectElem, START_DATE, END_DATE);
    startDateSelectElem.value = START_DATE;
    setSelectOptions(endDateSelectElem, START_DATE, END_DATE);
    endDateSelectElem.value = END_DATE;
}

