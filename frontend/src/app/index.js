import {END_DATE, PRECIPITATION, START_DATE, TEMPERATURE} from '../constants/index.js';

export function startApp(dataProvider, renderer, temperatureButton, precipitationButton, startDateSelect, endDateSelect) {
    const state = {
        graphType: TEMPERATURE,
        startDate: START_DATE,
        endDate: END_DATE
    };

    temperatureButton.addEventListener('click', () => {
        state.graphType = TEMPERATURE;

        temperatureButton.className = 'active';
        precipitationButton.className = '';

        onUpdate(state, dataProvider, renderer);
    });
    precipitationButton.addEventListener('click', () => {
        state.graphType = PRECIPITATION;

        precipitationButton.className = 'active';
        temperatureButton.className = '';

        onUpdate(state, dataProvider, renderer);
    });
    startDateSelect.addEventListener('change', (e) => {
        state.startDate = +e.target.value;
        if (state.startDate > state.endDate) {
            state.endDate = state.startDate;
            endDateSelect.value = state.endDate;
        }
        onUpdate(state, dataProvider, renderer);
    });
    endDateSelect.addEventListener('change', (e) => {
        state.endDate = +e.target.value;
        if (state.endDate < state.startDate) {
            state.startDate = state.endDate;
            startDateSelect.value = state.endDate;
        }
        onUpdate(state, dataProvider, renderer);
    });
    onUpdate(state, dataProvider, renderer);
}

/**
 * Вывзывается при каждом изменении состояния приложения и при его инициализации
 * @param state текущее состояние приложения
 * @param dataProvider поставщик данных
 * @param renderer метод отвечающий за визуализациию данных
 * @return {PromiseLike<T> | Promise<T>}
 */
function onUpdate(state, dataProvider, renderer) {
    let dataPromise = dataProvider(state).then((data) => {
        renderer(data, state);
    });
    return dataPromise;
}
