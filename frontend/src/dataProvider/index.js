const dataWorker = new Worker('scripts/webworkers/getDataWorker.js');
/**
 * Прокси для получения требуемых данных в подходящем формате,
 * полностью инкапсулирует логику хранения и полученния данных
 * @param state
 * @return {Promise<any>} масив данных для отображения на canvas
 */
export function getData(state) {
    const promise = new Promise((resolve) => {
        dataWorker.postMessage({type: 'GET_DATA', state: state});

        dataWorker.onmessage = (e) => {
            switch (e.data.type) {
                case 'RESULT_DATA': {
                    resolve(e.data.result);
                    break;
                }
                default: {
                    throw Error('Незивестное событие');
                }
            }
        };
    });

    return promise;
}
