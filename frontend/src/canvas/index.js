import {PRECIPITATION} from '../constants/index.js';

const prepareDataWorker = new Worker('scripts/webworkers/prepareDataForDrawWorker.js');

export function initCanvas(canvasElem) {
    const context = canvasElem.getContext('2d');

    const HEIGHT_OFFSET = 15;
    const WIDTH_OFFSET = 35;

    /**
     * Вывод осей и меток для графика
     * @param data
     * @param width
     * @param height
     * @param min
     * @param max
     */
    function drawGraphAxis(data, width, height, min, max) {
        const offsetForAxisLines = 2;
        drawLine(width + offsetForAxisLines, 0, width + offsetForAxisLines, height + offsetForAxisLines);
        drawLine(0, height + offsetForAxisLines, width + offsetForAxisLines, height + offsetForAxisLines);

        const labelCountY = 10;
        const stepSizeY = height / labelCountY;

        for (let i = 0; i <= labelCountY; i++) {

            let delta = max - min;

            const currentScale = (1 / labelCountY) * i;

            const label = (min + (delta * currentScale)).toFixed(2);
            context.fillText(label, width + 5, ((stepSizeY * i) * -1) + height);
        }

        const labelWidthX = 60;
        const labelCountX = width / labelWidthX;
        const stepSizeX = data.length / labelCountX;
        for (let i = 0, j = data.length; i < j; i += stepSizeX) {
            let temporary = data.slice(i, i + stepSizeX);
            context.fillText(temporary[0].key, temporary[0].x, height + HEIGHT_OFFSET, labelWidthX);
        }
    }

    function drawGraph(graphData) {

        const data = graphData.data;
        const max = graphData.max;
        const min = graphData.min;
        const width = graphData.width;
        const height = graphData.height;

        context.canvas.width = width + WIDTH_OFFSET;
        context.canvas.height = height + HEIGHT_OFFSET;

        context.clearRect(0, 0, width, height);

        context.save();
        context.strokeStyle = 'red';
        if (graphData.graphType === PRECIPITATION) {
            context.strokeStyle = 'blue';
        }

        drawGraphLine(data);
        context.restore();

        drawGraphAxis(data, width, height, min, max);
    }

    /**
     * Вывод полилайна по данным
     * @param data
     */
    function drawGraphLine(data) {
        context.beginPath();
        data.forEach((day) => {
            context.lineTo(day.x, day.y);
        });
        context.stroke();
    }

    /**
     * Вывод линии из точки А в точку Б
     * @param sourceX
     * @param sourceY
     * @param destnationX
     * @param destnationY
     */
    function drawLine(sourceX, sourceY, destnationX, destnationY) {
        context.beginPath();
        context.moveTo(sourceX, sourceY);
        context.lineTo(destnationX, destnationY);
        context.stroke();
    }

    return {
        draw: (data, state) => {
            const promise = new Promise((resolve) => {
                prepareDataWorker.postMessage({
                    type: 'PREPARE_DATA',
                    state: state,
                    data: data,
                    size: {
                        width: canvasElem.clientWidth - WIDTH_OFFSET,
                        height: canvasElem.clientHeight - HEIGHT_OFFSET
                    }
                });

                prepareDataWorker.onmessage = (e) => {
                    switch (e.data.type) {
                        case 'RESULT_DATA': {
                            resolve(e.data.result);
                            break;
                        }
                        default: {
                            throw Error('Незивестное событие');
                        }
                    }
                };
            });
            promise.then((dataForRender) => {
                drawGraph(dataForRender);
            });

        }
    };
}

