const express = require('express');
const path = require('path');
const router = require('./router');

const port = process.env.PORT || 3000;


const expressApp = express();

expressApp.use('/api', router);
expressApp.use('/static', express.static(path.resolve('./frontend/static')));
expressApp.use('/scripts', express.static(path.resolve('./frontend/src')));

expressApp.get('/', function (req, res) {
    res.sendFile(path.resolve('./frontend/index.html'));
});

expressApp.listen(port, (err) => {
    if (err) {
        return console.log(err);
    }
    return console.log(`server is listening on ${port}`);
});
