const data = require('../../data/meteo_data/precipitation');

function get(req, res) {
    res.json(data);
}

module.exports = get;