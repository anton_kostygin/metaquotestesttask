const data = require('../../data/meteo_data/temperature');

function get(req, res) {
    res.json(data);
}

module.exports = get;