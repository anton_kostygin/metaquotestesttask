const express = require('express');
const precipitation = require('../controller/precipitation');
const temperature = require('../controller/temperature');

const router = express.Router();

router.get('/precipitation', precipitation);
router.get('/temperature', temperature);

module.exports = router;